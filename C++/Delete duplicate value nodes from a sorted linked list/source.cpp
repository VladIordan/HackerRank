/*
  Remove all duplicate elements from a sorted linked list
  Node is defined as 
  struct Node
  {
     int data;
     struct Node *next;
  }
*/
Node* RemoveDuplicates(Node *head)
{
  // This is a "method-only" submission. 
  // You only need to complete this method.
  if (head == NULL) {
      return NULL;
  }
  if (head-> next == NULL){
      return head;
  }
  if (head-> data == (head -> next) -> data){
      return RemoveDuplicates(head->next);
  }
  Node* n = RemoveDuplicates(head->next);
  head-> next = n;
  return head;
}
