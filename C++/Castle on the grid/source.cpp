#include <bits/stdc++.h>
#include <queue>
using namespace std;

struct point {
    int x;
    int y;
};

point initialize(int x, int y){
    point p;
    p.x = x;
    p.y = y;
    return p;
}

bool isEqual(point p1, point p2){
    return ((p1.x == p2.x) && (p1.y == p2.y));
}

bool isValid(int x, int y, int n){
    return (x>=0 && x < n && y >= 0 && y < n);
}

queue <point> q;
int dx[] = {1,0,-1,0};
int dy[] = {0,1,0,-1};
int viz[101][101];

int minimumMoves(int n, vector <string> grid, int startX, int startY, int goalX, int goalY) {
    // Complete this function
    int i,j,X,Y,val,newX,newY;
    for (i=0; i<n; i++)
        for(j=0; j<n; j++)
        {
            if (grid[i][j] =='X')
                viz[i][j] = -1;
            else
                viz[i][j] = -2;
        }
    point start, finish;
    viz[startX][startY] = 0;
    start = initialize(startX, startY);
    finish = initialize(goalX, goalY);
    q.push(start);
    while (!q.empty())
    {
        X = q.front().x;
        Y = q.front().y;
        val = viz[X][Y];
        for (i=0; i<4 ;i++)
        {
            newX = q.front().x + dx[i];
            newY = q.front().y + dy[i];
            while (isValid(newX, newY, n) && viz[newX][newY] != -1)
            {
                point p0;
                if (viz[newX][newY] == -2){
                     p0 = initialize(newX,newY);
                     q.push(p0);
                     viz[newX][newY] = val+1;
                }
                newX = newX + dx[i];
                newY = newY + dy[i];
            }
        }
        q.pop();
    }
    return viz[finish.x][finish.y];
}

int main() {
    int n;
    cin >> n;
    vector<string> grid(n);
    for(int grid_i = 0; grid_i < n; grid_i++){
       cin >> grid[grid_i];
    }
    int startX;
    int startY;
    int goalX;
    int goalY;
    cin >> startX >> startY >> goalX >> goalY;
    int result = minimumMoves(n,grid, startX, startY, goalX, goalY);
    cout << result;
    return 0;
}
