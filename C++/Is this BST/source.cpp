/* Hidden stub code will pass a root argument to the function below. Complete the function to solve the challenge. Hint: you may want to write one or more helper functions.  

The Node struct is defined as follows:
	struct Node {
		int data;
		Node* left;
		Node* right;
	}
*/
	

    bool checkBST(Node* root) {
		vector <int> vec;
        vector <int> *v = &vec;
        inOrder(root, v);
        int n = v->size();
        int i;
        for (i = 0; i< n-1; i++)
            if (vec[i] >= vec[i+1])
                return false;
        return true;
	}

    void inOrder(Node* root, vector<int>* v){
        if (root -> left != NULL){
           inOrder(root -> left, v);
        }
        v->push_back(root->data);
        if (root -> right != NULL){
            inOrder(root -> right, v);
        }
    }