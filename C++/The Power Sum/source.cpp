#include <bits/stdc++.h>

using namespace std;

int calc(int a, int b)
{
    int i;
    int ans = 1;
    for (i=1; i<=b; i++)
    {
        ans = ans * a;
    }
    return ans;
}

int powerSum(int X, int N, int lim) {
    // Complete this function
    if (X == 0)
        return 1;
    if (lim == 1)
        return 0;
    int ans = 0;
    for (int i=1; i<lim; i++)
    {
        ans = ans + powerSum(X-calc(i,N), N, i);
    }
    return ans;
    
}

int main() {
    int X;
    cin >> X;
    int N;
    cin >> N;
    int lim = 1;
    while ( calc(lim,N) <= X){
        lim ++;
    }
    int result = powerSum(X, N, lim);
    cout << result << endl;
    return 0;
}
