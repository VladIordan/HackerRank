/*
   Reverse a doubly linked list, input list may also be empty
   Node is defined as
   struct Node
   {
     int data;
     Node *next;
     Node *prev;
   }
*/
Node* Reverse(Node* head)
{
    // Complete this function
    // Do not write the main method.
    Node* curr = head;
    while(curr != NULL){
        Node* aux;
        aux = curr -> next;
        curr-> next = curr -> prev;
        curr-> prev = aux;
        if( curr -> prev == NULL)
            return curr;
        curr = curr -> prev;
    }
    return curr;
    
}
