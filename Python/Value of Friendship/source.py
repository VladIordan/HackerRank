#!/bin/python3

import os
import sys

def makeGraph(n, friendships):
    graph = {}
    for i in range(1, n+1):
        graph[i] = []
    for (x,y) in friendships:
        graph[x].append(y)
        graph[y].append(x)
    return graph

def f(n):
    if n == 1:
        return 0
    # s(n^2) - s(n) = n*(n+1)*(2n+1)/6 - 3n*(n+1)/6
    # = n*(n+1)*(2n-2)/6
    # for n = 4 -> 4 * 5 * 6 / 6 = 20 
    return n * (n+1) * (n-1) // 3

#
# Complete the valueOfFriendsship function below.
#
def valueOfFriendsship(n, friendships):
    #
    # Write your code here.
    #
    graph = makeGraph(n, friendships)
    viz = {}
    for i in range(1, n+1):
        viz[i] = False
    q = []
    groups = []
    for i in range(1, n+1):
        if not viz[i]:
            q = [i]
            viz[i] = True
            size = 0
            while len(q) > 0:
                head = q[0]
                for x in graph[head]:
                    if not viz[x]:
                        q.append(x)
                        viz[x] = True
                size += 1
                q.pop(0)
            groups.append(size)
    groups = sorted(groups, reverse=True)
    total = 0
    for num in groups:
        total = total + f(num)
    left = m
    for num in groups:
        left = left - (num - 1)
        total = total + num * (num-1) * left
    return total
        

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input())

    for q_itr in range(q):
        nm = input().split()

        n = int(nm[0])

        m = int(nm[1])

        friendships = []

        for _ in range(m):
            friendships.append(list(map(int, input().rstrip().split())))

        result = valueOfFriendsship(n, friendships)

        fptr.write(str(result) + '\n')

    fptr.close()
