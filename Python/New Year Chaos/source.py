#!/bin/python

import sys

def minimumBribes(q):
    n = len(q)
    total = 0
    for i in range(n):
        if (q[i] - (i+1) > 2):
            print("Too chaotic")
            return
    poz = {}
    for i in range(n):
        poz[q[i]] = i
    for i in range(n):
        for j in range(q[i]+1, min(n+1,3+i)):
            if poz[j] < i:
                total += 1
            
    print(total)

if __name__ == "__main__":
    t = int(raw_input().strip())
    for a0 in xrange(t):
        n = int(raw_input().strip())
        q = map(int, raw_input().strip().split(' '))
        minimumBribes(q)

