#!/bin/python

import sys

def sansaXor(arr):
    n = len(arr)
    ans = 0
    for k in range(n):
        if (k+1) * (n-k) % 2 == 1:
            ans = ans ^ arr[k]
    return ans
    # Complete this function

if __name__ == "__main__":
    t = int(raw_input().strip())
    for a0 in xrange(t):
        n = int(raw_input().strip())
        arr = map(int, raw_input().strip().split(' '))
        result = sansaXor(arr)
        print result

