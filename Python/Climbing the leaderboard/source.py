#!/bin/python

import sys

def ranking(scores,n):
    num = 1
    leaderboard = []
    for i in range(1,n):
        leaderboard.append(num)
        if not scores[i] == scores[i-1]:
            num = num + 1
    leaderboard.append(num)
    return leaderboard
    

def bin_search(element, arr, leaderboard):
    if element >= arr[0]:
        return 1
    elif element < arr[-1]:
        return leaderboard[-1] +1
    beg = 0
    end = len(arr) - 1
    while beg < end -1:
        mid = (beg+end) / 2
        if arr[mid] == element:
            return leaderboard[mid]
        elif element < arr[mid]:
            beg = mid
        else:
            end = mid
    return leaderboard[end]
            
        

n = int(raw_input().strip())
scores = map(int,raw_input().strip().split(' '))
m = int(raw_input().strip())
alice = map(int,raw_input().strip().split(' '))
# your code goes here

leaderboard = ranking(scores,n)
for score in alice:
    place = bin_search(score,scores,leaderboard)
    print(place)
