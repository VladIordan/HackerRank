#!/bin/python

from __future__ import print_function

import os
import sys

global val

def DFS(node,graph):
    global val
    if len(graph[node]) == 0:
        val[node] = 1
        return 1
    ans = 1
    for x in graph[node]:
        ans = ans + DFS(x,graph)
    val[node] = ans
    return ans

if __name__ == '__main__':
    global val
    n, m = map(int, raw_input().split())
    graph = {}
    val = {}
    for i in range(1,n+1):
        graph[i] = []
        val[i] = 0
    for tree_itr in xrange(m):
        (x,y) = map(int, raw_input().split())
        graph[y].append(x)
    val[1] = DFS(1,graph)
    ans = 0
    for i in range(1,n+1):
        if val[i] % 2 == 0:
            ans += 1
    print(ans-1)
    
    
