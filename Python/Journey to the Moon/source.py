#!/bin/python3

import sys

def journeyToMoon(n, astronaut):
    graph= {}
    viz = {}
    for i in range(n):
        graph[i] = []
        viz[i] = False
    for (x,y) in astronaut:
        graph[x].append(y)
        graph[y].append(x)
    numComponents = 0
    groups = []
    for i in range(n):
        q = []
        if (not viz[i]):
            q.append(i)
            counter = 0
            viz[i] = True
            while(len(q) > 0):
                head = q[0]
                for x in graph[head]:
                    if not viz[x]:
                        q.append(x)
                        viz[x] = True
                counter += 1
                q.pop(0)
            groups.append(counter)
            numComponents += 1
    if (numComponents == 1):
        return 0
    ans = 0
    for x in groups:
        ans = ans + (n-x) * x
        n = n-x
    return ans
    
    # Complete this function

if __name__ == "__main__":
    n, p = input().strip().split(' ')
    n, p = [int(n), int(p)]
    astronaut = []
    for astronaut_i in range(p):
        astronaut_t = [int(astronaut_temp) for astronaut_temp in input().strip().split(' ')]
        astronaut.append(astronaut_t)
    result = journeyToMoon(n, astronaut)
    print(result)
