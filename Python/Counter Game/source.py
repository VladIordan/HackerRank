#!/bin/python

import sys

def counterGame(n):
    # Complete this function
    times = 0
    while(n % 2 == 0):
        n = (n >> 1)
        times += 1
    while(n > 1):
        times += n%2
        n = n >> 1
    if times%2 == 0:
        return "Richard"
    else:
        return "Louise"

if __name__ == "__main__":
    t = int(raw_input().strip())
    for a0 in xrange(t):
        n = long(raw_input().strip())
        result = counterGame(n)
        print result

