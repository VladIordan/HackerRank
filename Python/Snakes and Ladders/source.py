#!/bin/python3

import math
import os
import random
import re
import sys

def makeGraph(ladders, snakes):
    lad = {}
    sna = {}
    for (a,b) in ladders:
        lad[a] = b
    for (a,b) in snakes:
        sna[a] = b
    return (lad, sna)

def valid(x):
    return x >= 1 and x <= 100

# Complete the quickestWayUp function below.
def quickestWayUp(ladders, snakes):
    ladders, snakes = makeGraph(ladders, snakes)
    dis = {}
    for x in range(1,101):
        dis[x] =  -1
    q = [1]
    dis[1] = 0
    while len(q) > 0:
        head = q[0]
        if head == 100:
            return dis[head]
        for x in range(1,7):
            if valid(head+x) and dis[head+x] == -1:
                if head+x in snakes.keys() and dis[snakes[head+x]] == -1:
                    q.append(snakes[head+x])
                    dis[snakes[head+x]] = dis[head] + 1
                    dis[head+x] = dis[head] + 1
                elif head+x in ladders.keys() and dis[ladders[head+x]] == -1:
                    q.append(ladders[head+x])
                    dis[ladders[head+x]] = dis[head] + 1
                    dis[head+x] = dis[head] + 1
                else:
                    q.append(head+x)
                    dis[head+x] =  dis[head] + 1
        q.pop(0)
    return -1
            
    

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    t = int(input())

    for t_itr in range(t):
        n = int(input())

        ladders = []

        for _ in range(n):
            ladders.append(list(map(int, input().rstrip().split())))

        m = int(input())

        snakes = []

        for _ in range(m):
            snakes.append(list(map(int, input().rstrip().split())))

        result = quickestWayUp(ladders, snakes)

        fptr.write(str(result) + '\n')

    fptr.close()
