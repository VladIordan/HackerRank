#!/bin/python

import sys
#d[n] = d[n-1]*10 + s[n-1] * n
#ans = sum(d)
def substrings(balls):
    # Complete this function
    total = int(balls[0])
    d = []
    d.append(int(balls[0]))
    for i,ball in enumerate(balls[1:]):
        d.append(((d[i]*10) % (10**9 + 7) + int(ball)*(i+2)) % (10**9 + 7))
        total = (total + d[i+1]) % (10**9 + 7)
    return total

if __name__ == "__main__":
    balls = raw_input().strip()
    result = substrings(balls)
    print result
