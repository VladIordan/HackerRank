#!/bin/python3

import sys

def valid(x, y, matrix):
    n = len(matrix)
    m = len(matrix[0])
    return x>=0 and x<n and y>=0 and y<m

def DFS(current, target, matrix, viz):
    if(current == target):
        return (0,True)
    dx = [1,0,-1,0]
    dy = [0,1,0,-1]
    x = current[0]
    y = current[1]
    next = []
    for i in range(4):
        newX = x + dx[i]
        newY = y + dy[i]
        if valid(newX, newY, matrix):
            if not viz[newX][newY] and not matrix[newX][newY] == "X":
                next.append((newX,newY))
    if (len(next) > 1):
        answers = []
        for point in next:
            viz[point[0]][point[1]] = True
            answer = DFS(point, target, matrix, viz)
            if answer[1]:
                return (1 + answer[0], True)
        return(0, False)
    elif (len(next) == 1):
        viz[next[0][0]][next[0][1]] = True
        return DFS(next[0], target, matrix, viz)
    else:
        return (0, False)
                
    
def countLuck(matrix):
    # get start and finish
    finish = (-1,-1)
    start = (-1,-1)
    viz = []
    for i,line in enumerate(matrix):
        viz.append([])
        for j,elem in enumerate(line):
            viz[i].append(False)
            if(elem == '*'):
                finish = (i,j)
            if(elem == 'M'):
                start = (i,j)
    (x,y) = start
    viz[x][y] = True
    answer = DFS(start, finish, matrix, viz)
    return answer[0]
    # Complete this function

if __name__ == "__main__":
    t = int(input().strip())
    for a0 in range(t):
        n, m = input().strip().split(' ')
        n, m = [int(n), int(m)]
        matrix = []
        matrix_i = 0
        for matrix_i in range(n):
            matrix_t = str(input().strip())
            matrix.append(matrix_t)
        result = countLuck(matrix)
        k = int(input().strip())
        if result == k:
            print("Impressed")
        else:
            print("Oops!")