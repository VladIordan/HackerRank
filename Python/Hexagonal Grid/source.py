# Enter your code here. Read input from STDIN. Print output to STDOUT
tests = int(raw_input())
for t in range(tests):
    n = int(raw_input())
    line1 = raw_input().strip("\n")
    line2 = raw_input().strip("\n")
    a = [[],[]]
    for i in range(n):
        a[0].append(int(line1[i]))
        a[1].append(int(line2[i]))
    num_of_ones = 0
    ans = True
    for j in range(n):
        for i in range(2):
            if(a[1][j] == 1) and (a[0][j] == 1):
                if num_of_ones % 2 == 1:
                    ans = False
            elif(a[i][j] == 1):
                num_of_ones = num_of_ones + 1
                if num_of_ones % 2 == 0 and i == 0 and j>0:
                    if a[i+1][j-1] == 1:
                        ans = False
    if num_of_ones % 2 == 1 or ans == False:
        print("NO")
    else:
        print("YES")
                    
        