#!/bin/python

"""
Lexicographical order is often known as alphabetical order when dealing with strings. A string is greater than another string if it comes later in a lexicographically sorted list.

Given a word, create a new word by swapping some or all of its characters. This new word must meet two criteria:

It must be greater than the original word
It must be the smallest word that meets the first condition
Complete the function biggerIsGreater below to create and return the new string meeting the criteria. If it is not possible, return no answer.

Input Format

The first line of input contains , the number of test cases. 
Each of the next  lines contains .

Constraints

 will contain only letters in the range ascii[a..z].
Output Format

For each test case, output the string meeting the criteria. If no answer exists, print no answer.

Sample Input 0

5
ab
bb
hefg
dhck
dkhc
Sample Output 0

ba
no answer
hegf
dhkc
hcdk
Explanation 0

Test case 1: 
ba is the only string which can be made by rearranging ab. It is greater.
Test case 2: 
It is not possible to rearrange bb and get a greater string.
Test case 3: 
hegf is the next string greater than hefg.
Test case 4: 
dhkc is the next string greater than dhck.
Test case 5: 
hcdk is the next string greater than dkhc.

"""

import sys
from itertools import permutations

def biggerIsGreater(w):
    k = -1
    for i in range(len(w) -1):
        if w[i] < w[i+1]:
            k = i
    if k == -1:
        return "no answer"
    for i in range(k,len(w)):
        if w[k] < w[i]:
            l = i
    ans = w[0:k]
    ans += w[l]
    x = w[k+1:l]
    x += w[k]
    x += w[l+1:len(w)]
    for c in reversed(x):
        ans += c
    
    return ans
            
    # Complete this function

if __name__ == "__main__":
    T = int(raw_input().strip())
    for a0 in xrange(T):
        w = raw_input().strip()
        result = biggerIsGreater(w)
        print result

