#!/bin/python

import sys

def legitPos(n,x,y):
    return x >= 0 and y >= 0 and x < n and y<n

def BFS(n, a, b):
    viz = []
    for i in range(n):
        viz.append([])
        for j in range(n):
            viz[i].append(0)
    dx = [a,a,-a,-a,b,b,-b,-b]
    dy = [b,-b,b,-b,a,-a,a,-a]
    q = []
    viz[0][0] = 1
    q.append((0,0))
    while len(q) > 0:
        (x,y) = q[0]
        for i in range(8):
            new_x = x + dx[i]
            new_y = y + dy[i]
            if legitPos(n, new_x, new_y) and viz[new_x][new_y] == 0:
                viz[new_x][new_y] = viz[x][y] + 1
                q.append((new_x, new_y))
        q.pop(0)
    return viz[n-1][n-1] - 1

n = int(raw_input().strip())
ans = []
for i in range(n-1):
    ans.append([])
    for j in range(n-1):
        ans[i].append(0)

for i in range(n-1):
    for j in range(i,n-1):
        ans[i][j] = BFS(n, i+1, j+1)
        ans[j][i] = ans[i][j]

for i in range(n-1):
    print(" ".join(map(str,ans[i])))
# your code goes here
