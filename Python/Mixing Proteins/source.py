#!/bin/python3

import os
import sys

#
# Complete the pmix function below.
#
def decode(s):
    ltn = {}
    ltn['A'] = 0
    ltn['B'] = 1
    ltn['C'] = 2
    ltn['D'] = 3
    ans = []
    for x in s:
        ans.append(ltn[x])
    return ans
        
def encode(s):
    ntl = {}
    ntl[0] = 'A'
    ntl[1] = 'B'
    ntl[2] = 'C'
    ntl[3] = 'D'
    ans = ''
    for elem in s:
        ans = ans + ntl[elem]
    return ans

def logarithm(k):
    ans = 0
    n = 1
    while n <= k:
        ans += 1
        n *= 2
    ans -= 1
    return ans

def pmix(s, k):
    #
    # Write your code here.
    #    
    line = decode(s)
    n = logarithm(k)
    while(k>0):
        if k >= (1<<n):
            newline = []
            for i in range(len(line)):
                newline.append(line[i] ^ line[(i+(1<<n))%len(line)])
            line = newline
            k = k - (1<<n)
        n -= 1
    ans = encode(line)
    return ans

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    s = input()

    result = pmix(s, k)

    fptr.write(result + '\n')

    fptr.close()
