#!/bin/python3

import sys

def hackerlandRadioTransmitters(x, k):
    # Complete this function
    x = sorted(x)
    ans = 1
    start = x[0]
    t = x[0]
    for a in x[1:]:
        if a <= start + k:
            t = a
        elif a > t + k:
            ans += 1
            t = a
            start = a
    return ans
            
        

if __name__ == "__main__":
    n, k = input().strip().split(' ')
    n, k = [int(n), int(k)]
    x = list(map(int, input().strip().split(' ')))
    result = hackerlandRadioTransmitters(x, k)
    print(result)
