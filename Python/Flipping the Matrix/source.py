# Enter your code here. Read input from STDIN. Print output to STDOUT
a = []
t = int(raw_input())
for t0 in range(t):
    a = []
    n = int(raw_input())
    for i in range(2*n):
        arr = map(int,raw_input().split())
        a.append(arr)
    s = 0
    for i in range(n):
        for j in range(n):
            s += max(max(a[i][j],a[i][2*n-1-j]), max(a[2*n-1-i][j],a[2*n-1-i][2*n-1-j]))
    print(s)