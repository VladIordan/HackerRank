#!/bin/python

import sys

def cmp(x, y):
    if x[0] > y[0]:
        return -1
    elif x[0] == y[0]:
        return x[1] < y[1]
    else:
        return +1

def maximumSum(a, m):
    # Complete this function
    arr = []
    s = 0
    arr.append((m,0))
    for i,x in enumerate(a):
        s = (s + x) % m
        arr.append((s,i+1))
    #print(arr)
    arr = sorted(arr, cmp)
    #print(arr)
    ans = 0
    for i,x in enumerate(arr[1:]):
        if x[1] < arr[i][1]:
            possibleSum = arr[i][0] - x[0]
        else:
            possibleSum = x[0] - arr[i][0]
        possibleSum = possibleSum % m
        if possibleSum > ans:
     #       print(possibleSum)
            ans = possibleSum
    return ans
        
if __name__ == "__main__":
    q = int(raw_input().strip())
    for a0 in xrange(q):
        n, m = raw_input().strip().split(' ')
        n, m = [int(n), long(m)]
        a = map(long, raw_input().strip().split(' '))
        result = maximumSum(a, m)
        print result

