#!/bin/python3

import sys
from collections import Counter

def counter(s1):
    c = Counter(s1)
    ans = []
    for key in c.keys():
        ans.append((key,c[key]))
    return sorted(ans)


def sherlockAndAnagrams(s):
    L = len(s)
    ans = 0
    dic = {}
    for l in range(1,L+1):
        for i in range(L-l+1): 
            s1 = s[i:i+l]
            c1 = str(counter(s1))
            if c1 in dic.keys():
                dic[c1] += 1
            else:
                dic[c1] = 1
    #print(dic)
    for key in dic:
        ans = ans + int (dic[key] * (dic[key] - 1) / 2)
        #if dic[key] >= 2:
        #    print(key)
    return ans
                

    # Complete this function

q = int(input().strip())
for a0 in range(q):
    s = input().strip()
    result = sherlockAndAnagrams(s)
    print(result)
