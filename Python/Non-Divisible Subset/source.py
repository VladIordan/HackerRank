#!/bin/python

from collections import Counter
import sys

[n,m] = map(int,raw_input().split())
arr = map(int,raw_input().split())
for i in range(n):
    arr[i] = arr[i] % m
ans = 0
dic = Counter(arr)
if m%2 == 0:
    if dic[m/2] > 0:
        ans = ans + 1
if dic[0] > 0:
    ans = ans + 1
for i in range(1,(m-1)/2+1):
    ans = ans + max(dic[i],dic[m-i])
print ans