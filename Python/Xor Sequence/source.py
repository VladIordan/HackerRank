#!/bin/python

import sys

#a[4k] = 4k
#a[4k+1] = 1
#a[4k+2] = 4k+3
#a[4k+3] = 0


def calculateA(x):
    y = x - (x%4)
    cicles = y/4
    ans = 0
    if cicles%2 == 1:
        ans = 2
    for i in range(x+1-y):
        if i == 0:
            ans = ans ^ y
        elif i == 1:
            ans = ans ^ 1
        elif i == 2:
            ans = ans ^ (y+3)
        else:
            ans = ans ^ 0
    return ans

Q = int(raw_input().strip())
for a0 in xrange(Q):
    L,R = raw_input().strip().split(' ')
    L,R = [long(L),long(R)]
    print(calculateA(L-1)^calculateA(R))
    
