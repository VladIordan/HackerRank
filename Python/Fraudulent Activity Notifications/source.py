#!/bin/python3

import sys

def activityNotifications(expenditure, d):
    # Complete this function
    freq = [0] * 201
    answer = 0
    n = len(expenditure)
    for i in range(d):
        freq[expenditure[i]] += 1
    for i in range(d, n):
        total = 0
        m1 = -1
        m2 = -1
        for j in range(201):
            total += freq[j]
            if d % 2 == 1:
                if total >= (d+1) / 2:
                    median = j * 2
                    break
            else:
                if total >= d / 2 and m1 == -1:
                    m1 = j
                if total >= d / 2 + 1 and m2 == -1:
                    m2 = j
                    median = (m1+m2)
                    break
        if expenditure[i] >= median:
            answer += 1
        freq[expenditure[i-d]] -= 1
        freq[expenditure[i]] += 1
    return answer
                    
                    
            
            

if __name__ == "__main__":
    n, d = input().strip().split(' ')
    n, d = [int(n), int(d)]
    expenditure = list(map(int, input().strip().split(' ')))
    result = activityNotifications(expenditure, d)
    print(result)
